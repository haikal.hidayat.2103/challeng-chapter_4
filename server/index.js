const http = require("http");
const fs = require("fs");
const path = require("path");
const mime = require("mime");

const { PORT = 8000 } = process.env;
const cars = require("../data/cars.min.json");
const PUBLIC_DIRECTORY = path.join(__dirname, "..", "public");

// function to read html file in the public directory
function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

// When client request to http://localhost:8000 the function will be called
function onRequest(request, response) {
  if (
    request.url === "/" ||
    request.url.includes("sopir") ||
    request.url.includes("tanggal") ||
    request.url.includes("waktu") ||
    request.url.includes("jumlahPenumpang")
  ) {
    const html = getHTML("index.html");
    response.writeHead(200, { "Content-Type": "text/html" });
    response.end(html);
  } else if (request.url.match(".png$") || request.url.match(".jpg$")) {
    const filePath = path.join(__dirname, "..", "public", request.url);
    const fileStream = fs.createReadStream(filePath);
    const mimeType = mime.getType(filePath);
    response.writeHead(200, { "Content-Type": mimeType });
    fileStream.pipe(response);
  } else if (request.url.match(".js$") || request.url.match(".css$")) {
    const filePath = path.join(__dirname, "..", "public", request.url);
    const fileStream = fs.createReadStream(filePath, "UTF-8");
    const mimeType = mime.getType(filePath);
    response.writeHead(200, { "Content-Type": mimeType });
    fileStream.pipe(response);
  } else if (request.url === "/cars") {
    const cars = getHTML("sewa.html");
    response.writeHead(200, { "Content-Type": "text/html" });
    response.end(cars);
  } else if (request.url === "/api/cars") {
    response.setHeader("Content-Type", "application/json");
    response.writeHead(200);
    response.end(JSON.stringify(cars));
    return;
  } else {
    response.writeHead(404, { "Content-Type": "text/html" });
    response.end("No Page Found");
  }
}

// Create server
const server = http.createServer(onRequest);

// Run the server
server.listen(PORT, "0.0.0.0", () => {
  console.log(`Server is listening on port ${PORT}`);
});
