var xmlHttp = new XMLHttpRequest(); // Module Request
xmlHttp.open("GET", "http://localhost:8000/api/cars", false);
xmlHttp.send(null); // Request body null

var carsData = JSON.parse(xmlHttp.responseText); // Data from server

class Car {
  constructor(cars) {
    this.cars = cars;
  }
  filterCarAvailable() {
    return this.cars.filter((car) => car.available === true);
  }
  filterCarByUser() {
    var tanggal = document.getElementById("tanggal").value;
    var waktu = document.getElementById("waktu").value;
    var tnw = tanggal + waktu;
    var mWaktu = Date.parse(tnw);
    var jumlahPenumpang = document.getElementById("jumlahPenumpang").value;
    var sopir = document.getElementById("sopir").value;

    // Validator input user
    if (sopir == "undefined" && tanggal == "" && waktu == "undefined") {
      swal({
        title: "Please input the driver type, date and time",
        icon: "warning",
        dangerMode: true,
      });
      return;
    } else if (tanggal == "" && waktu == "undefined") {
      swal({
        title: "Please input the date & time",
        icon: "warning",
        dangerMode: true,
      });
      return;
    } else if (sopir == "undefined" && waktu == "undefined") {
      swal({
        title: "Please input the driver type and time",
        icon: "warning",
        dangerMode: true,
      });
      return;
    } else if (sopir == "undefined" && tanggal == "") {
      swal({
        title: "Please input the driver type and date",
        icon: "warning",
        dangerMode: true,
      });
      return;
    } else if (sopir == "undefined") {
      swal({
        title: "Please input the driver type",
        icon: "warning",
        dangerMode: true,
      });
      return;
    } else if (tanggal == "") {
      swal("Please input the date");
      swal({
        title: "Please input the date",
        icon: "warning",
        dangerMode: true,
      });
      return;
    } else if (waktu == "undefined") {
      swal({
        title: "Please input the time",
        icon: "warning",
        dangerMode: true,
      });
      return;
    }

    // Validator if the time is greater than now
    if (mWaktu < Date.now()) {
      swal({
        title: "Please select the date and time greater than now",
        icon: "warning",
        dangerMode: true,
      });
      return;
    }
    // display car available
    else if (jumlahPenumpang == "") {
      var list = this.cars.filter(
        (car) => car.availableAt <= tnw && car.available === true
      );
      return list;
    } else {
      var list = this.cars.filter(
        (car) =>
          car.capacity >= jumlahPenumpang &&
          car.available === true &&
          car.availableAt <= tnw
      );
      return list;
    }
  }
}

// initialize car
var cars = new Car(carsData);

// Get Element by ID carsData
var app = document.getElementById("carsData");
htmlData = "";

// Get Data from API
carsData = cars.filterCarAvailable();

// Loop Data
function getCars() {
  var htmlData = "";
  carsData = cars.filterCarByUser();
  if (carsData == "") {
    swal({
      title: "Cars not found",
      text: "Try another input",
      icon: "warning",
      dangerMode: true,
    });
  } else if (carsData == undefined) {
    return;
  } else {
    for (let index = 0; index < carsData.length; index++) {
      var car = carsData[index];
      htmlData += `     
        <div class="cars-card col-lg-4 col-md-6 col-sm-12 mt-lg-3 mt-md-3 mt-sm-3">
            <div class="card h-100">
              <div class="card-body">
              <div class="cars-body">
              <img
                  src="${car.image}"
                  class="card-img-top img-fluid mb-3 h-100"
                  alt="${car.manufacture} ${car.model}"
                  style="object-fit: cover"
                />
              </div>  
                <p class="card-text" style="font-weight: 400">
                  ${car.manufacture} ${car.model}
                </p>
                <h5 class="card-title mb-3 fw-bold">Rp ${car.rentPerDay} / hari</h5>
                <p class="card-text" style="text-align: justify; height: 60px">
                  ${car.description}
                </p>
                <p class="card-text">
                  <span
                    ><img src="./img/fi_users.png" class="me-2" alt="" /></span
                  >${car.capacity}
                </p>
                <p class="card-text">
                  <span
                    ><img
                      src="./img/fi_settings.png"
                      class="me-2"
                      alt="" /></span
                  >${car.transmission}
                </p>
                <p class="card-text">
                  <span
                    ><img
                      src="./img/fi_calendar.png"
                      class="me-2"
                      alt="" /></span
                  >Tahun ${car.year}
                </p>
                  <button
                    type="submit"
                    class="btn btn-main-section text-light mx-auto w-100"
                  >
                    Pilih Mobil
                  </button>
              </div>
            </div>
          </div>
            `;
    }
    app.innerHTML = htmlData;
  }
}
